package com.company;
/*
* Una Anonymous Inner Class es una clase Inner declada son un nombre
 * Se declaran e instancian al mismo tiempo
 * Se usan para anular un método en una clase o interfaz
 */

abstract class AnonymousInner{
    public abstract void metodo(); //Este es el método de una clase abstract de la cual queremos sobreescribir
}
public class Main {

    public static void main(String[] args) {
	AnonymousInner anonymous = new AnonymousInner() { //Creamos una nueva Anonymous Inner Class

        @Override // Se sobreescribe el método
        public void metodo() {
            System.out.print("Ejemplo de una clase anónima");
        }
    };
	anonymous.metodo(); //aquí se llama al método sobreescrito
    }
}
