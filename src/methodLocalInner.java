/**
 * Created by USRKAP on 24/07/2017.
 *
 En Java, podemos escribir una clase dentro de un método y éste será de tipo local. Al igual
 que las variables locales, el ámbito de la clase interna está restringido dentro del método.

 Method-local Inner Classes se puede instanciar sólo dentro del método donde se define la clase interna.
 */
public class methodLocalInner {

    void metodo(){   //aquí se declara el método
        class MethodInnerClass{    //esta es la clase local interna al método
            public void print(){
                System.out.print("Ejemplo de Method-Local Inner Class");
            }
        }
    }
}
