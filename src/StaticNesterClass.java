/**
 * Created by USRKAP on 24/07/2017.
 * Es una clase anidada miembro estático de una clase externa
 * Se accede sin instanciar la clase externa.
 * No tiene acceso a variables de la isntancia ni a método externos a la clase Outer
 *
 * */
public class StaticNesterClass {
    static class Nester{     // se crea la clase estática anidada
        public void metodo(){   //método para llamar a la clase anidada
            System.out.print("Esta es una Static Nested Class");
        }
    }

    public static void prueba(){
        StaticNesterClass.Nester nester = new StaticNesterClass.Nester(); // Se isntancia una clase anidada estática
        nester.metodo(); //Se accede a la clase estática mediante un método.
    }
}
