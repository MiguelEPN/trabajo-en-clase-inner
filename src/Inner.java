/**
 * Created by USRKAP on 24/07/2017.
 * Las Inner Class son clases dentro de otras clases.
 * A diferencia de una clase normal, una Inner puede ser privada
 * y una vez que declara una clase interna privada,
 * no se puede acceder desde un objeto fuera de la clase.
 */
public class Inner { //Clase Outer

    private class InnerClass{ //Clase Inner declarada como privada, solo puede ser accedida a través de un método
        public void print(){ //método para acceder a la clase
            System.out.println("Ejemplo de clase inner");
        }
    }
}
